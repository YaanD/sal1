﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SAL1
{
    class Student
    {
        // Define Student Properties
        int SID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public int Age { get; set; }

        // Define Student Variables
        int sId;
        string fName;
        string lName;
        int age;

        //define student number counter
        private static int studentnum = 1;


        //Define Constructor
        public Student()
        {
            
            char confirm = 'a';
            while (confirm == 'a')
            {
                SID = studentnum;
                Console.WriteLine($"Student Number: {SID}");
                Console.WriteLine("Please enter first name   ");
                FName = Console.ReadLine();
                Console.WriteLine("Please enter Student last name:   ");
                LName = Console.ReadLine();
                Console.WriteLine("Please enter student age:  ");
                Age = Convert.ToInt32(Console.ReadLine());


                while (confirm != 'y' && confirm != 'n')
                {
                    Console.WriteLine("Is this Correct: y/n?  ");
                    confirm = Console.ReadKey().KeyChar;
                }
                if (confirm == 'y')
                {
                    //Assign Collected variables to properties
                    this.sId = SID;
                    this.fName = FName;
                    this.lName = LName;
                    this.age = Age;

                    //Increment Student counter
                    studentnum++;
                    Console.WriteLine();
                    Console.WriteLine("Student Added.");
                }
                else
                {
                    confirm = 'a';
                }



            }
            
        }


        // Print Output
        public void PrintDetails()
        {
            Console.WriteLine($"Student number:   {sId}");
            Console.WriteLine($"Name:   {fName} {lName}");
            Console.WriteLine($"Age:  {age}");

        }




    }
}
