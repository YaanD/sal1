﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SAL1
{
    class Course
    {
        // Define Course Properties
        public int CourseNum { get; set; }
        public string CourseName { get; set; }
        public int NumStudents { get; set; }
        public int NumTeachers { get; set; }

        // Define Course Variables
        int courseNum;
        string courseName;
        int numStudents;
        int numTeachers;

        // Define Object Arrays
        Student[] studentObject;
        Teacher[] teacherObject;


        // Define counter
        private static int courseCounter = 1;

        //Constructor
        public Course()
        {
            // Set confirm checker
            char confirm = 'a';

            while (confirm == 'a')
            {
                CourseNum = courseCounter; 
                Console.WriteLine($"Course Code: AD{courseNum}");
                Console.WriteLine("Please enter the course name:  ");
                CourseName = Console.ReadLine();
                Console.WriteLine("Please enter the Number of Students to Enrol:  ");
                NumStudents = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Please Enter number of teaching Staff:  ");
                NumTeachers = Convert.ToInt32(Console.ReadLine());

                

                while (confirm != 'y' && confirm != 'n')
                {
                    Console.WriteLine("Is this information Correct?  y/n");
                    confirm = Console.ReadKey().KeyChar;
                    Console.WriteLine();

                    if (confirm == 'y')
                    {
                        courseNum = CourseNum;
                        courseName = CourseName;
                        numStudents = NumStudents;
                        numTeachers = NumTeachers;
                        courseCounter++;
                        Console.WriteLine("confirmed");
                    }
                    else
                    {
                        confirm = 'a';
                    }
                }

            }

            studentObject = new Student[numStudents];
            teacherObject = new Teacher[numTeachers];

            for (int i = 0; i < numStudents; i++)
            {
                studentObject[i] = new Student();
            }

            for (int j = 0; j < numTeachers; j++)
            {
                teacherObject[j] = new Teacher();
            }




        }

        //Print Output
        public void PrintDetails()
        {
            Console.WriteLine($"Course Code:   {courseNum}");
            Console.WriteLine($"Course Name:   {courseName}");

            Console.WriteLine("Enrolled Teachers:");
            foreach(Teacher teacherEntry in teacherObject)
            {
                Console.Write("    ");
                teacherEntry.PrintDetails();
            }

            Console.WriteLine("Enrolled Students:");
            foreach ( Student studentEntry in studentObject)
            {
                Console.Write("   ");
                studentEntry.PrintDetails();
            }

        }
    }
}
