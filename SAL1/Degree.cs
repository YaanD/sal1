﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SAL1
{
    class Degree
    {
        // Define Degree Properties
        public int DegreeNum { get; set; }
        public string DegreeName { get; set; }
        public int NumCourse { get; set; }

        // Define Private Variables
        int degreeNum;
        string degreeName;
        int numCourse;

        // Init Course Object Array
        Course[] course;

        //Define Degree Counter
        private static int degreeCounter = 1;


        // Constructor
        public Degree()
        {
            // set confirm checker
            char confirm = 'a';

            while (confirm == 'a')
            {
                DegreeNum = degreeCounter;
                Console.WriteLine($"Degree Code:  {DegreeNum}");
                Console.WriteLine("Enter Degree Name:  ");
                DegreeName = Console.ReadLine();
                Console.WriteLine("Enter the Number of associated Courses:  ");
                NumCourse = Convert.ToInt32(Console.ReadLine());

                // Confirm Correct?
                while (confirm != 'y' && confirm != 'n')
                {
                    Console.WriteLine("Are these Details Correct?  y/n");
                    confirm = Console.ReadKey().KeyChar;
                }

                // Assign values to variables if correct
                if (confirm == 'y')
                {
                    degreeNum = DegreeNum;
                    degreeName = DegreeName;
                    numCourse = NumCourse;
                    degreeCounter++;
                }
                else
                {
                    confirm = 'a';
                }

                // Create Required Number of Course Objects

                course = new Course[NumCourse];
                for (int i = 0; i < NumCourse; i++)
                {
                    course[i] = new Course();
                }
            }

        }


        public void PrintDetails()
        {
            Console.WriteLine($"Degree Code:   {DegreeNum}");
            Console.WriteLine($"Name:  {DegreeName}");
            foreach (Course entry in course)
            {
                entry.PrintDetails();
            }
        }

    }
}