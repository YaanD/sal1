﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SAL1
{
    class Teacher
    {
        //define teacher properties
        public static int TeacherNum = 1;
        public string FName { get; set; }
        public string LName { get; set; }
        public string TimeEmpl { get; set; }
        public int Salary { get; set; }

        int teacherNum;
        string fName;
        string lName;
        string timeEmpl;
        int salary;

        

        //Constructor
        public Teacher()
        {
            char confirm = 'a';

            while (confirm == 'a')
            {
                Console.WriteLine($"Staff ID Number:  {TeacherNum}");
                Console.WriteLine("Enter First Name:  ");
                FName = Console.ReadLine();
                Console.WriteLine("Enter Last Name:  ");
                LName = Console.ReadLine();
                Console.WriteLine("Enter Time Employed:  ");
                TimeEmpl = Console.ReadLine();
                Console.WriteLine("Enter Annual Salary:  ");
                Salary = Convert.ToInt32(Console.ReadLine());

                while (confirm != 'y' && confirm != 'n')
                {
                    Console.WriteLine("Is this information Correct?  y/n");
                    confirm = Console.ReadKey().KeyChar;
                       
                }

                if (confirm == 'y')
                {
                    teacherNum = TeacherNum;
                    fName = FName;
                    lName = LName;
                    timeEmpl = TimeEmpl;
                    salary = Salary;
                    teacherNum++;
                }
                else
                {
                    confirm = 'a';
                }
            }
        }

        public void PrintDetails()
        {
            Console.WriteLine($"Staff number:   {teacherNum}");
            Console.WriteLine($"Name:   {fName} {lName}");
            Console.WriteLine($"Time Employed:  {timeEmpl}");
            Console.WriteLine($"Annual Salary:  {salary}");
           
        }
    }

}
