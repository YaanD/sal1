﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SAL1
{
    class UProgram
    {
        // Define UProgram Properties
        public int PNumber { get; set; }
        public string PName { get; set; }
        public int NumDegree { get; set; }

        //Define UProgram Variables
        int pNumber;
        string pName;
        int numDegree;

        // Define Object Arrays
        Degree[] degreeObjects;


        //Init UProgram numerator
        private static int uProgNumber = 1;

        //Constructor
        public UProgram()
        {
            char confirm = 'a';
            while (confirm == 'a')
            {
                //Collect Program Details
                PNumber = uProgNumber;
                Console.WriteLine($"Program ID: AusD{PNumber}");
                Console.WriteLine("Please enter Program Name:  ");
                PName = Console.ReadLine();
                Console.WriteLine("Please enter Number of Associated Degrees: ");
                NumDegree = Convert.ToInt32(Console.ReadLine());
                
                //Confirm the Program Details

                while (confirm != 'y' && confirm != 'n')
                {
                    Console.WriteLine("Are these details Correct?   y/n:");
                    Console.WriteLine();
                    confirm = Console.ReadKey().KeyChar;
                    Console.WriteLine();
                }

                if (confirm == 'y')
                    {
                    //Pass entered details to variable
                    int pNumber = PNumber;
                    string pName = PName;
                    int numDegree = NumDegree;

                    // Add to program count
                    uProgNumber++;
                    }
                else
                {
                    confirm = 'a';
                }
                                
            }
            
            // Update Array to create Required number of Degrees.
            degreeObjects = new Degree[NumDegree];

            //Instantiate Course objects

            for (int i = 0; i < NumDegree; i++)
            {
                degreeObjects[i] = new Degree();
            }
        }

        //Print Details
        public void PrintDetails()
        {
            Console.WriteLine($"Program Code:   {pNumber}");
            Console.WriteLine($"Program Name:   {pName}");
            
            foreach(Degree degreeEntry in degreeObjects)
            {
                Console.WriteLine("DEGREE INFORMATION");
                Console.Write("    ");
                degreeEntry.PrintDetails();
            }

        }
    }
}
